package com.xcast.gameoflife;

import java.util.Arrays;

public class GameOfLife
{
    private int WIDTH, HEIGHT;
    private boolean[][] matrix;
    private final static char EMPTY = ' ', ALIVE='\u25A0';//(char)220;
    private int aliveCount = 0;
    private boolean stable = false;

    public GameOfLife(int WIDTH, int HEIGHT)
    {
        this.HEIGHT = HEIGHT;
        this.WIDTH = WIDTH;
        matrix = new boolean[WIDTH][HEIGHT];
        for (int i = 0; i < WIDTH; i++)
        {
            Arrays.fill(matrix[i],false);
        }
    }

    public void startPopulation(Point ...individuals)
    {
        for(Point p : individuals)
        {
            matrix[p.y][p.x] = true;
        }
        aliveCount = individuals.length;
    }

    public void iterate()
    {
        boolean somethingChanged = false;
        for (int col = 0; col < WIDTH; col++)
        {
            for (int row = 0; row < HEIGHT; row++)
            {
                int neighborCount = neighborCount(col,row);
                boolean live = matrix[col][row];
                if(live)
                {
                    if(neighborCount<2)
                    {
                        matrix[col][row]=false;
                        aliveCount--;
                        somethingChanged = true;
                    }
                    else if(neighborCount>3)
                    {
                        matrix[col][row]=false;
                        aliveCount--;
                        somethingChanged = true;
                    }
                }
                else if(neighborCount==3)
                {
                    matrix[col][row]=true;
                    aliveCount++;
                    somethingChanged = true;
                }
            }
        }
        stable = !somethingChanged;
    }

    public void print()
    {
        for (int col = 0; col < HEIGHT; col++)
        {
            String aux="";
            for (int row = 0; row < WIDTH; row++)
            {
                aux+=getCharAt(col,row)+" ";
            }
            System.out.println(aux);
        }
    }

    private char getCharAt(int col, int row)
    {
        if(col<0 || col>=WIDTH) {return EMPTY;}
        if(row<0 || row>=HEIGHT) {return EMPTY;}
        return matrix[col][row]?ALIVE:EMPTY;
        //return matrix[col][row]?(char)(neighborCount(col,row)+'0'):EMPTY;
    }

    private boolean getBoolAt(int col, int row)
    {
        if(col<0 || col>=WIDTH) {return false;}
        if(row<0 || row>=HEIGHT) {return false;}
        return matrix[col][row];
    }

    private int neighborCount(int col, int row)
    {
        int count = 0;
        if(getBoolAt(col+1,row)) {count+=1;}
        if(getBoolAt(col-1,row)) {count+=1;}
        if(getBoolAt(col,row+1)) {count+=1;}
        if(getBoolAt(col,row-1)) {count+=1;}
        if(getBoolAt(col+1,row+1)) {count+=1;}
        if(getBoolAt(col+1,row-1)) {count+=1;}
        if(getBoolAt(col-1,row+1)) {count+=1;}
        if(getBoolAt(col-1,row-1)) {count+=1;}
        //System.out.printf("count (%d,%d): %d%n",col,row,count);
        return count;
    }

    public boolean[][] getMatrix()
    {
        return matrix;
    }

    public int getAliveCount()
    {
        return aliveCount;
    }

    public boolean isStable()
    {
        return stable;
    }
}
